#!/bin/sh
# Copyright 2009 Iakov Davydov
#
# Distributed under the terms of the GNU General Public License v3
#

URL="http://omploader.org/upload"
INTERVAL=5

upload ()
{
    curl -q -F "file1=@$1" $URL -o - \
     | grep -E '(View file: <a href="v([A-Za-z0-9+\/]+)">|Slow down there, cowboy\.)'
}

slow_down ()
{
    echo "$1" | grep -iq "Slow down there, cowboy\."
}

if [ ! -n "$1" ]
then
    echo "Usage: $(basename $0) file [file ...]"
    exit 10
fi

if ! which curl > /dev/null
then
    echo "Cannot find curl. Exiting."
    exit 9
fi

n=0
max=$#
for arg in "$@"; do
    RESULT=$(upload "$arg")
    if slow_down "$RESULT"
    then
	INTERVAL=$((INTERVAL+60))
	echo "# Slowing down to ${INTERVAL}s."
	sleep $INTERVAL
	RESULT="$(upload "$arg")"
    fi
    if slow_down "$RESULT" 
    then
	echo "$arg: too fast permanent error."
	exit 2
    fi

    if [ ! -n "$RESULT" ]
    then
	echo "$arg: empty reply."
    fi
    [ $# -ne 1 ] && printf "$arg:"
    resurl="$(echo "$RESULT" | sed 's/^.*\(http:\/\/.*\)<.*$/\1/')"
    echo " $resurl : $resurl/$arg"

    n=$((n+1))
    [ $n -ge $max ] || sleep $INTERVAL
done
